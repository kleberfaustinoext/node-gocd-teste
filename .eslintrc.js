module.exports = {
  env: {
    commonjs: true,
    es6: true,
    node: true,
  },
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2019,
  },
  rules: {
    'global-require': 0,
    'linebreak-style': 0,
    'semi': 2
  },
  overrides: [
    {
      "files": ["src/**/*.js"],
    }
  ]
};
